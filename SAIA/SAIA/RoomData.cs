﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAIA
{
	public class Interactable
	{
		public string Name, Contains, Dialog;
		public Conversation Conversation;

		public Interactable( string name, string contains, string dialog )
		{
			this.Name = name;
			this.Contains = contains;
			this.Dialog = dialog;
		}
	}

	public class RoomConnection
	{
		public string Name, Alias;

		public RoomConnection( string name, string alias )
		{
			this.Name = name;
			this.Alias = alias;
		}
	}

	public class StandingPoint
	{
		public string Id;
		public Bitmap Bitmap;

		public StandingPoint( string id, Bitmap bitmap )
		{
			this.Id = id;
			this.Bitmap = bitmap;
		}
	}

	public class RoomData
	{
		public Map<Interactable> Interactables = new Map<Interactable>();
		public Map<RoomConnection> ConnectedRooms = new Map<RoomConnection>();
		public Map<StandingPoint> StandingPoints = new Map<StandingPoint>();
		public Map<string> RoomVariables = new Map<string>();

		public string Name;

		public RoomData( string name ) { Name = name; }
	}
}
