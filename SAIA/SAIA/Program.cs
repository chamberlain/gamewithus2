﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SAIA
{
	public class Program
	{
		public static readonly Stopwatch Stopwatch = Stopwatch.StartNew();
		public static Game Game { get; private set; }
		public static bool KeepAlive = true;

		public static void Main( string[] args )
		{
			Program.Game = new Game();
		}
	}
}
