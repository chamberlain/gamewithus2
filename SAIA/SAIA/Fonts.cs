﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Text;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SAIA
{
	public static class Fonts
	{
		private static PrivateFontCollection collection = new PrivateFontCollection();
		public static FontFamily ConsoleFont;

		static Fonts()
		{
			ConsoleFont = LoadFont( "Data/Fonts/pixelmix.ttf" );
		}

		static FontFamily LoadFont( string path )
		{
			//if( File.Exists( path ) ) return new FontFamily( path, collection );
			//else
			return FontFamily.GenericMonospace;
		}
	}
}
