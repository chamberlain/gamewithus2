﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAIA
{
	public static class GameData
	{
		public static readonly Map<Conversation> Conversations = new Map<Conversation>();
		public static readonly Map<RoomData> Rooms = new Map<RoomData>();
		public static readonly Map<InventoryItem> Items = new Map<InventoryItem>();
		// 
		public static readonly Map<Bitmap> BitmapCache = new Map<Bitmap>();
		public static bool saia_activated;
		public static int game_step;
		public static int oxygenTime;
	}
}
