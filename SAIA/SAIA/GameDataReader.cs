﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SAIA
{
	public class GameDataReader
	{
		private const string PREFIX = "../../";
		private const string PATH_IMAGES = PREFIX + "Data/Images/";
		private const string PATH_ROOMS = PREFIX + "Data/Rooms/";
		private const string DIALOG_PATH = PREFIX + "Data/Dialog.xml";
		private const string ITEMS_PATH = PREFIX + "Data/Items.xml";
		private const string IMAGE_PATH = PREFIX + "Data/Images/";

		private static void loadImages()
		{
			// Load every room file
			foreach( string filename in Directory.GetFiles( IMAGE_PATH, "*.png" ) )
			{
				var image = Path.GetFileName( filename );
				GameData.BitmapCache[image] = new Bitmap( IMAGE_PATH + image );
			}

			foreach( string filename in Directory.GetFiles( IMAGE_PATH + "Inv/", "*.png" ) )
			{
				var image = Path.GetFileName( filename );
				GameData.BitmapCache[image] = new Bitmap( IMAGE_PATH + "Inv/" + image );
			}
		}

		private static void loadItems()
		{
			XmlDocument xml = new XmlDocument();
			xml.Load( ITEMS_PATH );

			// Get elements
			XmlNodeList itemLists = xml.GetElementsByTagName( "ItemList" );
			if( itemLists.Count > 0 )
			{
				XmlNode itemList = itemLists[0];

				foreach( XmlNode item in itemList.ChildNodes )
				{
					string name = item.Attributes["name"].Value;
					string image = item.Attributes["image"].Value;
					GameData.Items[name] = new InventoryItem( name, GameData.BitmapCache[image] );
				}
			}
		}

		private static void loadDialog()
		{
			XmlDocument xml = new XmlDocument();
			xml.Load( DIALOG_PATH );

			// Get elements
			XmlNodeList dialogItems = xml.GetElementsByTagName( "Dialog" );
			if( dialogItems.Count > 0 )
			{
				XmlNode dialog = dialogItems[0];

				foreach( XmlNode conversation in dialog.ChildNodes )
				{
					string id = conversation.Attributes["id"].Value;

					List<string> messages = new List<string>();
					foreach( XmlNode message in conversation.ChildNodes )
						messages.Add( message.InnerText.Trim() );

					GameData.Conversations[id] = new Conversation( messages.ToArray() );
				}
			}
		}

		private static void loadRoom( string path, string filename )
		{
			XmlDocument xml = new XmlDocument();
			xml.Load( path + filename );

			string roomName = Path.GetFileNameWithoutExtension( filename );
			var room = GameData.Rooms[roomName] = new RoomData( roomName );

			// Read Connections
			XmlNodeList connectionsItems = xml.GetElementsByTagName( "Connections" );
			if( connectionsItems.Count > 0 )
			{
				foreach( XmlNode connection in connectionsItems[0].ChildNodes )
				{
					string name = connection.Attributes["name"].Value;
					string alias = ( connection.Attributes.GetNamedItem( "alias" ) != null ) ? connection.Attributes["alias"].Value : "";
					room.ConnectedRooms[name] = new RoomConnection( name, alias );
				}
			}

			// Read Interactables
			XmlNodeList interactabesItems = xml.GetElementsByTagName( "Interactables" );
			if( interactabesItems.Count > 0 )
			{
				foreach( XmlNode interactable in interactabesItems[0].ChildNodes )
				{
					string name = interactable.Attributes["name"].Value;
					string contains = interactable.Attributes["contains"].Value;
					if( interactable.Attributes.GetNamedItem( "dialog" ) != null )
					{
						string dialog = interactable.Attributes["dialog"].Value;
						room.Interactables[name] = new Interactable( name, contains, dialog );
					}
					else room.Interactables[name] = new Interactable( name, contains, "" );

				}
			}

			// Read Standing Points
			XmlNodeList standingItems = xml.GetElementsByTagName( "StandingPoints" );
			if( standingItems.Count > 0 )
			{
				foreach( XmlNode standing in standingItems[0].ChildNodes )
				{
					string id = standing.Attributes["id"].Value;
					string image = standing.Attributes["image"].Value;

					Console.WriteLine( id + " || " + image );
					var point = new StandingPoint( id, image == "" ? null : GameData.BitmapCache[image] );

					// Store attributes
					foreach( XmlAttribute attribute in standing.Attributes )
					{
						if( !room.RoomVariables.ContainsKey( attribute.Name ) )
							room.RoomVariables[attribute.Name] = attribute.Value;
					}

					room.StandingPoints[id] = point;
				}
			}
		}

		internal static void LoadData()
		{
			loadImages();

			// Load every room file
			foreach( string filename in Directory.GetFiles( PATH_ROOMS ) )
				loadRoom( PATH_ROOMS, Path.GetFileName( filename ) );

			// Load Dialog File
			loadDialog();

			// Load Items Files
			loadItems();
		}
	}
}