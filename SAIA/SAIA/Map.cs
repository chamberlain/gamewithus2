﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAIA
{
	public class Map<T> : IEnumerable<T>
	{
		private Dictionary<string, T> items = new Dictionary<string, T>();

		public T this[string v]
		{
			set { items[v] = value; }
			get { return items[v]; }
		}

		public int Count { get { return items.Count; } }

		public bool ContainsKey( string k ) { return items.ContainsKey( k ); }

		public Dictionary<string, T>.ValueCollection GetValues() { return items.Values; }

		public IEnumerator<T> GetEnumerator() { return items.Values.GetEnumerator(); }

		IEnumerator IEnumerable.GetEnumerator()
		{
			return items.Values.GetEnumerator();
		}
	}
}
