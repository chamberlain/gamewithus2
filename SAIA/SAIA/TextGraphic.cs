﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAIA
{
	public class TextGraphic : Graphic
	{
		public string Text { get; set; }

		public Rectangle InnerBounds { get; set; }

		private Rectangle _Bounds;
		public override Rectangle Bounds
		{
			get { return _Bounds; }
			set
			{
				_Bounds = value;

				//_bounds = value;
				InnerBounds = new Rectangle( _Bounds.Location, _Bounds.Size );
				InnerBounds.Inflate( -Game.PADDING, -Game.PADDING );
			}
		}

		private int _lineCount = 1;
		int LineCount
		{
			get { return _lineCount; }

			set
			{
				_lineCount = value;
				if( font != null ) font.Dispose();
				font = new Font( Fonts.ConsoleFont, InnerBounds.Height / (float) _lineCount, FontStyle.Regular, GraphicsUnit.Pixel );
			}
		}

		private Font font;

		public TextGraphic( Rectangle bounds, int lineCount, string text = "" )
			: base( bounds )
		{
			Text = text;
			LineCount = lineCount;
		}

		public override void Draw( Graphics g )
		{
			g.FillRectangle( Palette.TextBox, Bounds );
			g.DrawString( Text, font, Palette.TextBoxColor, Bounds );
		}
	}
}
