﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SAIA
{
	public class InventoryItem
	{
		public Bitmap Bitmap { get; set; }
		public string Name { get; set; }

		public InventoryItem( string name, Bitmap bitmap )
		{
			Name = name;
			Bitmap = bitmap;
		}
	}

	public class Game : Form
	{
		public const int PADDING = 15;
		//
		public const int TEXTBOX_LINES = 15;
		public const int TEXTBOX_SIZE = 300;
		//
		public const int COMMAND_SIZE = 25;
		//
		public const int INV_ITEM_SIZE = 50;
		//
		public Dictionary<string, RoomData> Rooms = new Dictionary<string, RoomData>();
		public List<InventoryItem> Inventory = new List<InventoryItem>();

		private Commander Commander;
		private TextGraphic DescriptionBox, RoomBox;
		private ImageGraphic BackgroundGraphic;
		private ImageGraphic[] InventoryBoxes;

		Stack<RoomData> roomStack = new Stack<RoomData>();
		bool shipPower = false;

		// Game State
		RoomData CurrentRoom;

		StandingPoint CurrentPoint;

		public Game()
		{
			// Setup Form
			ClientSize = new Size( 1200, 650 );
			FormBorderStyle = FormBorderStyle.FixedSingle;
			DoubleBuffered = true;
			MaximizeBox = false;
			Visible = true;
			Activate();
			GameData.game_step = 0;
			GameData.oxygenTime = 30;

			// X to close
			FormClosed += ( o, e ) => Program.KeepAlive = false;

			// 
			var rDescription = new Rectangle( PADDING, ClientSize.Height - TEXTBOX_SIZE - PADDING, ClientSize.Width - INV_ITEM_SIZE - PADDING * 3, TEXTBOX_SIZE );
			var rCommand = new Rectangle( PADDING, ClientSize.Height - COMMAND_SIZE - PADDING, ClientSize.Width - INV_ITEM_SIZE - PADDING * 3, COMMAND_SIZE );
			var rRoomLabel = new Rectangle( PADDING, PADDING, ClientSize.Width / 2 - PADDING, PADDING * 2 );
			var rInventory = CalculateInventoryRects( 9 );

			// Read XML Files
			GameDataReader.LoadData();
			CurrentRoom = GameData.Rooms["01_Entrance"];

			Inventory.Add( GameData.Items["flashlight"] );
			Inventory.Add( GameData.Items["oxygen tank"] );
			Inventory.Add( GameData.Items["sidearm"] );

			// Graphic Objects
			Commander = new Commander( this, rCommand );
			DescriptionBox = new TextGraphic( rDescription, TEXTBOX_LINES );
			RoomBox = new TextGraphic( rRoomLabel, 1 );
			InventoryBoxes = new ImageGraphic[rInventory.Length];
			for( int i = 0; i < 9; i++ )
			{
				InventoryBoxes[i] = new ImageGraphic( rInventory[i] );
			}

			BackgroundGraphic = new ImageGraphic( new Rectangle( 0, 0, ClientSize.Width, ClientSize.Height ) );

			// Render Thread
			mainLoop(); // 
		}

		// 
		Rectangle[] CalculateInventoryRects( int size )
		{
			Rectangle[] rectangles = new Rectangle[size];

			int topCenterOffset = ( ClientSize.Height - PADDING - ( rectangles.Length * ( PADDING + INV_ITEM_SIZE ) ) ) / 2;
			for( int i = 0; i < rectangles.Length; i++ )
			{
				rectangles[i] = new Rectangle(
					ClientSize.Width - INV_ITEM_SIZE - PADDING,
					topCenterOffset + PADDING + i * ( PADDING + INV_ITEM_SIZE ),
					INV_ITEM_SIZE, INV_ITEM_SIZE );
			}

			return rectangles;
		}

		// 
		private void mainLoop()
		{
			while( Program.KeepAlive )
			{
				Application.DoEvents();
				Thread.Sleep( 1000 / 100 );
				Refresh();
			}

			Application.Exit(); // Bye!
		}

		protected override void OnPaintBackground( PaintEventArgs e ) { }

		protected override void OnPaint( PaintEventArgs e )
		{
			var g = e.Graphics;
			g.CompositingQuality = CompositingQuality.HighQuality;
			g.InterpolationMode = InterpolationMode.HighQualityBicubic;
			g.SmoothingMode = SmoothingMode.HighQuality;
			g.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;

			g.Clear( Palette.Background );

			// Assign text to render
			if( CurrentPoint == null ) CurrentPoint = CurrentRoom.StandingPoints["1"];
			RoomBox.Text = CurrentRoom.Name + ( CurrentPoint.Id == "1" ? "" : ( " - " + CurrentPoint.Id ) );

			// Assign images to render
			BackgroundGraphic.Bitmap = CurrentPoint.Bitmap;
			for( int i = 0; i < Inventory.Count; i++ )
				InventoryBoxes[i].Bitmap = Inventory[i].Bitmap;

			// Draw Text
			BackgroundGraphic.Draw( g );

			if( !shipPower )
				g.FillRectangle( Palette.Darken, 0, 0, ClientSize.Width, ClientSize.Height );

			RoomBox.Draw( g );
			DescriptionBox.Draw( g );
			Commander.Draw( g );
			for( int i = 0; i < InventoryBoxes.Length; i++ )
				InventoryBoxes[i].Draw( g );
		}

		internal void action_examine( string target )
		{
			string desc = "";
			if( target == "" )
			{
				desc += "You see: \n";
				foreach( var interactable in CurrentRoom.Interactables )
				{
					if( interactable.Contains != "" )
					{
						desc += "\t- " + interactable.Name + "\n";
					}
				}
				if( GameData.saia_activated )
				{
					desc += "\t- saia\n";
				}
			}
			else
			{
				if( CurrentRoom.Interactables.ContainsKey( target ) )
				{
					var inter = CurrentRoom.Interactables[target];
					if( inter.Contains == "" )
					{
						desc += "You already picked that up!\n";
					}
					else
					{
						if( "plotInfo" == inter.Contains )
						{
							if( inter.Dialog == "" ) desc += "plotInfo and not a dialog...?";
							else desc += GameData.Conversations[inter.Dialog].Messages[0];
						}
						else if( "nothing" == inter.Contains )
						{
							desc += "You examine \"" + target + "\" and find nothing of interest.\n";
						}
						else
						{
							desc += "You examine \"" + target + "\" and find \"" + inter.Contains + "\".\n";
						}
					}
				}
				else if( GameData.saia_activated && target == "saia" )
				{
					//plot info
				}
				else
				{
					desc += "Sorry, examine what?";
				}
			}

			DescriptionBox.Text = desc;
		}

		internal void action_goto( string target )
		{
			string desc = "";
			if( target == "" )
			{
				desc += "You can go to: \n";
				foreach( var room in CurrentRoom.ConnectedRooms )
					desc += "\t- " + room.Alias + "\n";
			}
			else
			{
				// messy code
				bool moved = false;
				foreach( var room in CurrentRoom.ConnectedRooms )
				{
					if( room.Alias == target )
					{
						roomStack.Push( CurrentRoom );
						CurrentRoom = GameData.Rooms[room.Name];
						if( CurrentRoom.StandingPoints.Count <= 1 )
						{
							CurrentPoint = CurrentRoom.StandingPoints["1"];
						}
						else
						{
							CurrentPoint = CurrentRoom.StandingPoints["middle"];
						}
						moved = true;
						break;
					}
				}

				if( !moved )
				{
					desc += "Can't go there.";
				}
				else
				{
					GameData.oxygenTime--;
					if( GameData.oxygenTime <= 0 )
					{
						desc = "You lose!\n";
					}
				}
				DescriptionBox.Text = desc;
			}
		}

		internal void action_go_back( string target )
		{
			string desc = "";

			if( roomStack.Count > 0 )
			{
				CurrentRoom = roomStack.Pop(); if( CurrentRoom.StandingPoints.Count <= 1 )
				{
					CurrentPoint = CurrentRoom.StandingPoints["1"];
				}
				else
				{
					CurrentPoint = CurrentRoom.StandingPoints["middle"];
				}
				desc = "Stepped into previous room.";
			}
			else
			{
				desc = "You need to finish exploring the ship! Don't abandon your quest now!";
			}

			DescriptionBox.Text = desc;
		}

		internal void action_pickup( string target )
		{
			string desc = "";
			if( target == "" ) desc += "You can't pick up nothing.";
			else
			{
				bool found = false;
				foreach( var inter in CurrentRoom.Interactables )
				{
					if( Inventory.Count < InventoryBoxes.Length )
					{
						if( inter.Contains == target )
						{
							desc += "You obtain \"" + inter.Contains + "\".\n";
							InventoryItem item = GameData.Items[inter.Contains];
							Inventory.Add( item );

							// Marks as 'picked up'
							inter.Contains = "";
							found = true;
							break;
						}
					}
					else
					{
						found = true;
						desc += "Inventory full, destroy an item to make room.";
						break;
					}
				}

				if( !found )
				{
					desc += "That isn't there!";
				}
			}
			DescriptionBox.Text = desc;
		}

		internal void action_move( string target )
		{
			string desc = "";


			if( target == "" && CurrentRoom.StandingPoints.Count > 1 )
			{
				desc += "You can go: \n";
				foreach( var standingpoint in CurrentRoom.StandingPoints )
				{
					desc += "\t " + standingpoint.Id + "\n";
				}
			}
			else if( CurrentRoom.StandingPoints.Count > 1 )
			{
				CurrentPoint = CurrentRoom.StandingPoints[target];
			}
			else
			{
				desc += "You can only stand here.\n";
			}
			DescriptionBox.Text = desc;

		}

		internal void action_destroy( string invSlot )
		{
			string desc = "";
			if( int.Parse( invSlot ) > 9 || int.Parse( invSlot ) < 0 || Inventory.Count > int.Parse( invSlot ) )
			{
				desc += "You have indicated an incorrect item slot.\n";
			}
			else
			{
				Inventory.RemoveAt( int.Parse( invSlot ) );
				desc += "You have removed from slot " + invSlot;
			}
			DescriptionBox.Text = desc;
		}

		internal void action_use( string invSlot )
		{
			string desc = "";
			if( int.Parse( invSlot ) > 9 || int.Parse( invSlot) < 0 || Inventory.Count > int.Parse( invSlot ) )
			{
				desc += "You have indicated an incorrect item slot.\n";
			}
			else
			{
				var item = Inventory[int.Parse(invSlot)];
				if(item.Name.Contains("tank"))
				{
					GameData.oxygenTime += 5;
				}
			}
		}

		internal void action_unknown()
		{ DescriptionBox.Text = "Sorry, unknown action"; }

		internal void action_dance( string x )
		{
			if( x == "like there is no tomorrow" )
				DescriptionBox.Text = "A sudden urge takes over you body as you begin to seize and convulse violently to a rhythm that is very much in your head.";
			else
				DescriptionBox.Text = "You flail your arms around in way you thought might resemble dancing. It does't, but good try.";
		}
	}
}
