﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAIA
{
	public abstract class Graphic
	{
		public virtual Rectangle Bounds { get; set; }

		public Graphic( Rectangle bounds ) { Bounds = bounds; }

		public abstract void Draw( Graphics g );
	}
}
