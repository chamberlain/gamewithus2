﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAIA
{
	public class ImageGraphic : Graphic
	{
		public Bitmap Bitmap = null;

		public ImageGraphic( Rectangle bounds )
			: base( bounds ) { }

		public override void Draw( Graphics g )
		{
			if( Bitmap == null ) g.DrawRectangle( Palette.TextBoxColor, Bounds );
			else g.DrawImage( Bitmap, Bounds );
		}
	}
}
