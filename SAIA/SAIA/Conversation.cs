﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAIA
{
	public class Conversation
	{
		public string[] Messages { get; private set; }

		public Conversation( string[] messages )
		{
			Messages = messages;
		}
	}
}
