﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAIA
{
	public struct Paint
	{
		Color Color;
		Brush Brush;
		Pen Pen;

		public Paint( string hex )
		{
			Color = Color.FromArgb( Int32.Parse( hex, NumberStyles.HexNumber ) );
			Brush = new SolidBrush( Color );
			Pen = new Pen( Brush, 1F );
		}

		public static implicit operator Pen( Paint d ) { return d.Pen; }

		public static implicit operator Brush( Paint d ) { return d.Brush; }

		public static implicit operator Color( Paint d ) { return d.Color; }
	}

	public static class Palette
	{
		public static readonly Paint Background = new Paint( "AA222222" );
		//
		public static readonly Paint TextBoxColor = new Paint( "FFAAFFFF" );
		public static readonly Paint TextBox = new Paint( "D0334444" );
		public static readonly Paint Darken = new Paint( "EE000000" );
	}
}
