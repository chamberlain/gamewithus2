﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SAIA
{
	/**
	 * "This is my favorite game on the citadel"
	 */
	public class Commander : TextGraphic
	{
		private string buffer = "";
		private bool showCaret = false;
		private bool windowFocused = true;
		private long _caretTime;

		public Game Game { get; private set; }

		private Dictionary<string, Action<string>> commands;

		public Commander( Game form, Rectangle bounds )
			: base( bounds, 1, "" )
		{
			Game = form;
			
			//
			commands = new Dictionary<string, Action<string>>()
			{
				{ "go back", Game.action_go_back },
				{ "go to", Game.action_goto },
				{ "goto", Game.action_goto },
				{ "pickup", Game.action_pickup },
				{ "pick up", Game.action_pickup },
				{ "grab", Game.action_pickup },
				{ "take", Game.action_pickup },
				{ "examine", Game.action_examine },
				{ "search", Game.action_examine },
				{ "dance", Game.action_dance },
				{ "move", Game.action_move },
				{ "destroy", Game.action_destroy },
				{ "talk", Game.action_examine },
				{ "use", Game.action_use }
			};

			// 
			_caretTime = Program.Stopwatch.ElapsedMilliseconds;
			form.KeyDown += form_Key;
			// 
			form.GotFocus += ( o, e ) => windowFocused = true;
			form.LostFocus += ( o, e ) => windowFocused = false;
			form.Activated += ( o, e ) => windowFocused = true;
			form.Deactivate += ( o, e ) => windowFocused = false;
		}

		private void form_Key( object sender, KeyEventArgs e )
		{
			Keys key = e.KeyCode;
			int keyValue = (int) key;

			// Delete last character
			if( buffer.Length > 0 && key == Keys.Back )
			{
				buffer = buffer.Substring( 0, buffer.Length - 1 );
			}
			else if( key == Keys.Enter )
			{
				// Trim Buffer
				buffer = buffer.Trim();

				// If there is content in buffer
				if( buffer.Length > 0 )
				{
					bool foundAction = false;
					foreach( KeyValuePair<string, Action<string>> cmd in commands )
					{
						if( buffer.StartsWith( cmd.Key ) )
						{
							var target = cleanString( buffer.Substring( cmd.Key.Length ).Trim() );
							cmd.Value( target );
							foundAction = true;
						}
					}

					// Nope, unreconized action
					if( !foundAction )
						Game.action_unknown();

					buffer = "";
				}
			}
			else if( key == Keys.Escape )
			{
				Program.KeepAlive = false;
			}
			else
			{
				if( ( keyValue >= 0x30 && keyValue <= 0x39 ) // numbers
				 || ( keyValue >= 0x41 && keyValue <= 0x5A ) // letters
				 || ( keyValue >= 0x60 && keyValue <= 0x69 ) // numpad
				 || key == Keys.Space )
				{
					buffer += Char.ToLower( ( (char) key ) );
				}
			}
		}

		// Regex 'collapse whitepspace'
		private string cleanString( string m ) { return Regex.Replace( m, @"\s+", " " ); }

		public override void Draw( Graphics g )
		{
			if( Program.Stopwatch.ElapsedMilliseconds - _caretTime > 500 )
			{
				_caretTime = Program.Stopwatch.ElapsedMilliseconds;
				showCaret = !showCaret;
			}

			//
			Text = buffer + ( windowFocused && showCaret ? "|" : "" );
			base.Draw( g );
		}
	}
}
